<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * Class Trip
 *
 * @property integer $id
 * @property string $title
 * @property string $start_date
 * @property string $end_date
 * @property integer $vehicle_id
 * @property integer $driver_id
 * @property integer $route_id
 * @property float $price
 */
	class Trip {}
}

namespace App\Models{
/**
 * Class Traveler
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property integer $reputation
 * @property integer $added_by
 */
	class Traveler {}
}

namespace App\Models{
/**
 * Class Vehicle
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property string $registration_number
 * @property integer $capacity
 */
	class Vehicle {}
}

namespace App\Models{
/**
 * Class Route
 *
 * @property integer $id
 * @property string $name
 */
	class Route {}
}

namespace App\Models{
/**
 * Class LoginAttempt
 *
 * @property integer $id
 * @property string $ip_address
 * @property string $login
 * @property integer $time
 */
	class LoginAttempt {}
}

namespace App\Models{
/**
 * Class City
 *
 * @property integer $id
 * @property string $name
 */
	class City {}
}

namespace App\Models{
/**
 * Class TripRow
 *
 * @property integer $trip_id
 * @property integer $traveler_id
 * @property integer $total_price
 * @property integer $paid
 * @property integer $seats
 * @property string $from_city
 * @property string $to_city
 */
	class TripRow {}
}

namespace App\Models{
/**
 * Class RouteStop
 *
 * @property integer $id
 * @property integer $route_id
 * @property integer $city_id
 */
	class RouteStop {}
}

namespace App\Models{
/**
 * Class Group
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 */
	class Group {}
}

namespace App{
/**
 * App\User
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
	class User {}
}

