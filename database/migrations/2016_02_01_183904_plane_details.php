<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlaneDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trip_rows', function (Blueprint $table) {
            $table->string('plane_connection');
            $table->string('plane_time');
            $table->string('details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trip_rows', function (Blueprint $table) {
            $table->dropColumn('plane_connection');
            $table->dropColumn('plane_time');
            $table->dropColumn('details');
        });
    }
}
