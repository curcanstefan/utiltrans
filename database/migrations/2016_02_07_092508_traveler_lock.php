<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TravelerLock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trip_rows', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('locked');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trip_rows', function (Blueprint $table) {
            $table->dropColumn('id');
            $table->dropColumn('locked');
        });
    }
}
