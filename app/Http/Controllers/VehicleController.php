<?php

namespace App\Http\Controllers;

use App\Http\Models\Vehicle;

use App\Http\Requests;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user_roles:user', [
            'only' => [
                'get',
            ]
        ]);
        $this->middleware('user_roles:admin', ['only' => [
            'set',
        ]]);
    }

    public function get($id = null)
    {
        if ($id) {
            return response()->json([Vehicle::find($id)]);
        }

        return response()->json(Vehicle::orderBy('name')->get());
    }

    public function set(Request $request)
    {
        $vehicleData = [
            'id'       		        => (int) $request->input('id'),
            'name'                  => $request->input('name'),
            'capacity'    		    => (int) $request->input('capacity'),
            'registration_number'   => $request->input('registration_number'),
        ];

        $vehicle = Vehicle::firstOrNew([
            'id' => $vehicleData['id'],
        ]);
        $vehicle->fill($vehicleData);
        $vehicle->save();

        return response()->json([$vehicle->id]);
    }
}
