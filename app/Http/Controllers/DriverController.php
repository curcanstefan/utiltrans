<?php

namespace App\Http\Controllers;

use App\Http\Models\Driver;
use App\Http\Requests;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DriverController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user_roles:user', [
            'only' => [
                'get',
            ]
        ]);
        $this->middleware('user_roles:admin', ['only' => [
            'set',
        ]]);
    }

    public function get($id = null)
    {
        if ($id) {
            return response()->json([Driver::find($id)]);
        }

        $drivers = Driver::leftJoin('trips', 'drivers.id', '=', 'trips.driver_id')
            ->groupBy('drivers.id')
            ->orderBy(DB::raw('COUNT(trips.id)'), 'DESC')
            ->get([
                'drivers.*',
            ]);

        return response()->json($drivers);
    }

    public function set(Request $request)
    {
        $driverData = [
            'id'        => (int) $request->input('id'),
            'name'      => $request->input('name'),
            'phone'     => $request->input('phone'),
            'active'    => (bool) $request->input('active'),
        ];

        $driver = Driver::firstOrNew([
            'id' => $driverData['id'],
        ]);
        $driver->fill($driverData);
        $driver->save();

        return response()->json([$driver->id]);
    }
}
