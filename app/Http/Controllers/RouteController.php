<?php

namespace App\Http\Controllers;

use App\Http\Models\Route;

use App\Http\Requests;

class RouteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user_roles:user', [
            'only' => [
                'get',
            ]
        ]);
    }

    public function get($id = null)
    {
        if ($id) {
            return response()->json([Route::find($id)]);
        }

        return response()->json(Route::orderBy('name')->get());
    }
}
