<?php

namespace App\Http\Controllers;

use App\Http\Models\Traveler;
use App\Http\Models\Trip;
use Illuminate\Http\Request;

use App\Http\Requests;

class TripsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user_roles:user', [
            'only' => [
                'index',
                'get',
                'getByDay',
            ]
        ]);
        $this->middleware('user_roles:admin', ['only' => [
            'set',
        ]]);
    }

    public function index()
    {
        return view('trip');
    }

    public function get($id = null)
    {
        $trips = Trip::getTrips($id);
        foreach ($trips as $key => $trip) {
            $travelers = Traveler::getTravelersByTripId($trip->id);
            $trip->travelers = $travelers;
            $trip->travelers_count = Traveler::getTravelersCountByTripId($trip->id);
        }

        echo json_encode($trips);
    }

    public function getByDay($day = null)
    {
        $trips = Trip::getByDay($day);
        foreach ($trips as $key => $trip) {
            $travelers = Traveler::getTravelersByTripId($trip->id);
            $trip->travelers = $travelers;
            $trip->travelers_count = Traveler::getTravelersCountByTripId($trip->id);
        }

        echo json_encode($trips);
    }

    public function set(Request $request)
    {
        $startTimestamp = strtotime($request->input('start_date').' '.$request->input('start_time'));
        $endTimestamp   = strtotime($request->input('bulk_end_date').' '.$request->input('start_time'));

        $trip           = Trip::firstOrNew(['id' => $request->input('id')]);
        $oldTimestamp   = strtotime($trip->start_date);
        $tripId         = $this->setTrip($trip, $startTimestamp, $request);

        $dayInSeconds   = 60 * 60 * 24;

        var_dump($request->input('save_bulk'));
        if ($request->input('save_bulk') && $endTimestamp > $startTimestamp) {
            for ($day = $startTimestamp + $dayInSeconds; $day <= $endTimestamp; $day+=$dayInSeconds) {
                $oldTimestamp += $dayInSeconds;
                $trip = Trip::firstOrNew([
                    'start_date' => date('Y-m-d H:i:s', $oldTimestamp),
                ]);
                $this->setTrip($trip, $day, $request);
            }
        }

        return response()->json($tripId);
    }

    protected function setTrip(Trip $trip, $day, Request $request)
    {
        $dateFormat = 'Y-m-d H:i:s';
        $endDate    = date($dateFormat, $day + (3*60*60));
        $startDate  = date($dateFormat, $day);

        $data = [
            'title'         => $request->input('title'),
            'start_date'    => $startDate,
            'end_date'      => $endDate,
            'price'    		=> $request->input('price'),
            'vehicle_id'    => $request->input('vehicle'),
            'driver_id'    	=> $request->input('driver'),
            'route_id'    	=> $request->input('route'),
        ];
        $trip->fill($data);
        $trip->save();

        return response()->json($trip->id);
    }
}
