<?php

namespace App\Http\Controllers;

use App\Http\Models\Traveler;
use App\Http\Models\Trip;
use App\Http\Models\TripRow;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class TravelerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user_roles:user', [
            'only' => [
                'get',
                'set',
                'lock',
                'delete',
        ]]);
        $this->middleware('user_roles:admin', ['only' => [
            'move',
        ]]);
    }

    public function get($id = null)
    {
        $isAdmin = Auth::user()->is('admin');
        $travelers = Traveler::getTravelers($id);

        foreach ($travelers as $traveler) {
            $traveler->added_by = $isAdmin ? $traveler->agency->name : '';
            unset($traveler->agency);
        }

        return response()->json($travelers);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function set(Request $request)
    {
        $trip = Trip::getTrips($request->input('trip_id'))[0];
        if ($trip->locked) {
            return response()->json(['message' => 'trip locked'], 403);
        }

        $traveler = Traveler::firstOrCreate([
                'name'      => $request->input('traveler_name'),
                'phone'    	=> $request->input('phone'),
                'added_by'	=> Auth::user()->id,
            ]);

        $seats = (int) $request->input('seats');
        $seats = $seats ? $seats : 1;

        $tripRowData = [
            'id'       		=> $request->input('trip_row_id'),
            'trip_id'       => $request->input('trip_id'),
            'traveler_id'   => $traveler->id,
            'seats'   		=> $seats,
            'total_price'   => $trip->price * $seats,
            'paid'    		=> $request->input('amount_paid'),
            'from_city'    	=> $request->input('departure_city'),
            'to_city'    	=> $request->input('destination_city'),
            'plane_connection'  => $request->input('plane_connection'),
            'plane_time'    	=> $request->input('plane_time'),
            'details'    	    => $request->input('details'),
        ];
        $overwrite = $request->input('_method') == 'PUT';
        $tripRowId = TripRow::setRow($tripRowData, $overwrite);

        if ($tripRowId === 'duplicate') {
            return response()->json(['message' => 'duplicate'], 400);
        }

        return response()->json([$tripRowId]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function lock(Request $request)
    {
        $trip = Trip::getTrips($request->input('trip_id'))[0];
        if ($trip->locked) {
            return response()->json(['message' => 'trip locked'], 403);
        }

        $traveler = Traveler::findOrFail($request->input('traveler_id'));

        if (!Auth::user()->is('admin') && Auth::user()->id != $traveler->added_by) {
            return response()->json(['message' => 'not your traveler'], 403);
        }

        $result = TripRow::lockRow([
            'trip_id'       => $request->input('trip_id'),
            'traveler_id'   => $traveler->id,
            'lock'    	    => filter_var($request->input('lock'), FILTER_VALIDATE_BOOLEAN),
        ]);

        return response()->json(['message' => (bool) $result]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Request $request)
    {
        $trip = Trip::getTrips($request->input('trip_id'))[0];
        if ($trip->locked) {
            return response()->json('trip locked', 403);
        }

        $traveler = Traveler::findOrFail((int) $request->input('traveler_id'));

        if (!Auth::user()->is('admin') && Auth::user()->id != $traveler->added_by) {
            return response()->json('not your traveler', 403);
        }

        $result = TripRow::where('trip_id', '=', $trip->id)
            ->where('traveler_id', '=', $traveler->id)
            ->first()
            ->delete();

        return response()->json($result);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function move(Request $request)
    {
        $trip = Trip::getTrips($request->input('from_trip_id'))[0];
        if ($trip->locked) {
            return response()->json('trip locked', 403);
        }

        $travelers = array_map(function($el) {return (int) $el; }, $request->input('travelers'));
        $toTripId = (int) $request->input('to_trip_id');

        $tripRows = TripRow::where('trip_id', '=', $trip->id)
            ->whereIn('traveler_id', $travelers)
            ->get();

        foreach ($tripRows as $row) {
            $row->trip_id = $toTripId;
            $row->save();
        }

        return response()->json($trip->id);
    }
}
