<?php

namespace App\Http\Controllers;

use Artisan;
use Exception;
use Response;

class EnvController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user_roles:admin', [
            'only' => [
            ]

        ]);
        $this->middleware('user_roles:developer', ['only' => [
            'date',
            'migrateStatus',
            'migrate',
            'pwd',
        ]]);
    }

    public function migrateStatus()
    {
        try {
            echo '<br>init migrate:status...<br/>';
            $statusResponse = Artisan::call('migrate:status');
            echo 'status response: '.$statusResponse;
        } catch (Exception $e) {
            Response::make($e->getMessage(), 500);
        }
//        $output = null;
//        exec('php ../artisan migrate:status', $output);
//        return $output;
    }

    public function migrate()
    {
        try {
            echo '<br>init with app tables migrations...';
            Artisan::call('migrate');
            echo '<br>done with app tables migrations';
        } catch (Exception $e) {
            Response::make($e->getMessage(), 500);
        }
//        $output = null;
//        exec('php ../artisan migrate', $output);
//        return $output;
    }

    public function date()
    {
        $output = null;
        exec('date', $output);
        return $output;
    }

    public function pwd()
    {
        $output = null;
        exec('pwd', $output);
        return $output;
    }
}
