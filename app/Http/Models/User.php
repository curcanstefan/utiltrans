<?php

namespace App\Http\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Http\Models\Role', 'users_roles');
    }

    public function travelers()
    {
        return $this->hasMany('App\Http\Models\Traveler', 'added_by', 'id');
    }

    /**
     * @param $roleName
     *
     * @return bool
     */
    public function is($roleName)
    {
        foreach ($this->roles()->get() as $role)
        {
            if (strtolower($role->name) == strtolower($roleName))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $roleName
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function addRole($roleName)
    {
        return $this->roles()
            ->save(
                Role::where('name', '=', $roleName)
                    ->first()
            );
    }
}
