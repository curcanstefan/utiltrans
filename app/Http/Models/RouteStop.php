<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RouteStop
 */
class RouteStop extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'route_id',
        'city_id'
    ];

    protected $guarded = [];

        
}