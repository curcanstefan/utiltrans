<?php

namespace App\Http\Models;

/**
 * Class TripRow
 *
 * @property integer    id
 * @property integer    trip_id
 * @property integer    traveler_id
 * @property integer    total_price
 * @property integer    paid
 * @property integer    seats
 * @property string     from_city
 * @property string     to_city
 * @property string     plane_connection
 * @property string     plane_time
 * @property string     details
 */
class TripRow extends \Eloquent
{

    public $timestamps = false;

    protected $fillable = [
        'trip_id',
        'traveler_id',
        'total_price',
        'paid',
        'seats',
        'from_city',
        'to_city',
        'plane_connection',
        'plane_time',
        'details'
    ];

    protected $guarded = [];

    public static function setRow($input, $overwrite = false)
    {
        $data = [
            'trip_id'           => $input['trip_id'],
            'traveler_id'       => $input['traveler_id'],
            'total_price'       => $input['total_price'],
            'seats'    		    => $input['seats'],
            'paid'    		    => $input['paid'],
            'from_city'    	    => $input['from_city'],
            'to_city'    	    => $input['to_city'],
            'plane_connection'  => $input['plane_connection'],
            'plane_time'        => $input['plane_time'],
            'details'           => $input['details'],
        ];
        $trip = TripRow::where([
            'trip_id' 		=> $input['trip_id'],
            'traveler_id'	=> $input['traveler_id'],
        ])->first();

        if ($trip && $trip->locked && !Auth::user()->is('admin')) {
            // update locked traveler only if admin
            return $trip->id;
        }

        if ($trip && $overwrite) {
            $trip->update($data);
            return $trip->id;
        }

        if ($trip) {
            // do not overwrite if user did not edit a traveler
            return 'duplicate';
            // throw exception 'duplicate'
        }

        return TripRow::create($data)->trip_id;
    }

    public static function lockRow($input)
    {
        $tripRow = TripRow::where([
            'trip_id' 		=> $input['trip_id'],
            'traveler_id'	=> $input['traveler_id'],
        ])->first();
        $tripRow->locked = (bool) $input['lock'];
        $tripRow->save();
        return $tripRow->id;
    }
}