<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent;
use Illuminate\Database\Query\Builder;

/**
 * Class Trip
 */
class Trip extends \Eloquent
{

    public $timestamps = false;

    protected $fillable = [
        'title',
        'start_date',
        'end_date',
        'vehicle_id',
        'driver_id',
        'route_id',
        'price',
    ];

    protected $guarded = [
        'locked',
    ];

    public static function getByDay($day = null)
    {
        $query = Trip::getTripsQuery()
            ->whereDate('start_date', '=', $day);
        return self::setLocked($query->get());
    }

    /**
     * @param null $id
     * @return array
     */
    public static function getTrips($id = null)
    {
        $query = self::getTripsQuery();
        if ($id) {
            $query->where('trips.id', $id);
        }

        return self::setLocked($query->get());
    }

    /**
     * @return Builder
     */
    protected static function getTripsQuery()
    {
        return \DB::table('trips')
            ->join('vehicles', 'trips.vehicle_id', '=', 'vehicles.id')
            ->join('drivers', 'trips.driver_id', '=', 'drivers.id')
            ->join('routes', 'trips.route_id', '=', 'routes.id')
            ->select('trips.id',
                'trips.title',
                'trips.start_date',
                'trips.end_date',
                'trips.vehicle_id',
                'trips.price',
                'vehicles.name AS vehicle_name',
                'vehicles.capacity AS vehicle_capacity',
                'trips.driver_id',
                'drivers.name AS driver_name',
                'trips.route_id',
                'routes.name AS route_name')
            ->orderBy('trips.start_date');
    }

    /**
     * @param array $trips
     * @return array
     */
    protected static function setLocked(array $trips)
    {
        $now = date('Y-m-d H:i:s');
        foreach ($trips as $trip) {
            if ($trip->end_date < $now) {
                $trip->locked = true;
            }
            if (!isset($trip->locked)) {
                $trip->locked = false;
            }
        }

        return $trips;
    }

    public function isLocked()
    {
        $now = date('Y-m-d H:i:s');
        return $this->end_date < $now ? true : false;
    }
}