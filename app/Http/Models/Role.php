<?php

namespace App\Http\Models;

class Role extends \Eloquent
{
    /**
     * Set timestamps off
     */
    public $timestamps = false;

    /**
     * Get users with a certain role
     */
    public function users()
    {
        return $this->belongsToMany('App\Http\Models\User', 'users_roles');
    }
 }