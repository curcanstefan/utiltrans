<?php

namespace App\Http\Models;

/**
 * Class Driver
 * @package App\Http\Models
 */
class Driver extends \Eloquent
{
    protected $fillable = [
        'name',
        'phone',
        'active',
    ];
}
