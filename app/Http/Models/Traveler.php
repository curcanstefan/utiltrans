<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class Traveler
 */
class Traveler extends \Eloquent
{

    public $timestamps = false;

    protected $fillable = [
        'name',
        'phone',
        'reputation',
        'added_by',
        'locked',
    ];

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agency()
    {
        return $this->hasOne('App\Http\Models\User', 'id', 'added_by');
    }

    /**
     * @param null $id
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function getTravelers($id = null)
    {
        $addedBy = Auth::user()->is('admin') ? 0 : Auth::user()->id;

        $query = Traveler::with('agency');
        if ($addedBy) {
            $query->where('travelers.added_by', '=', $addedBy);
        }
        if ($id) {
            $query->where('travelers.id', '=', $id);
        }

        return $query->get();
    }

    public static function getTravelersByTripId($tripId = null, $travelerId = null)
    {
        $addedBy = Auth::user()->is('admin') ? 0 : Auth::user()->id;

        $query = \DB::table('trips')
            ->join('trip_rows', 'trips.id', '=', 'trip_rows.trip_id')
            ->join('travelers', 'trip_rows.traveler_id', '=', 'travelers.id')
            ->join('users', 'users.id', '=', 'travelers.added_by')
            ->select('travelers.id', 'travelers.name', 'travelers.phone', 'travelers.reputation',
                'trip_rows.total_price', 'trip_rows.paid', 'trip_rows.seats', 'trip_rows.from_city',
                'trip_rows.plane_connection', 'trip_rows.plane_time', 'trip_rows.details',
                'trip_rows.locked', 'trip_rows.to_city', 'users.name AS added_by')
            ->orderBy('travelers.id');
        if ($addedBy) {
            $query->where('travelers.added_by', '=', $addedBy);
        }
        if ($tripId !== null) {
            $query->where('trips.id', '=', $tripId);
        }
        if ($travelerId !== null) {
            $query->where('travelers.id', '=', $travelerId);
        }

        return $query->get();
    }

    public static function getTravelersCountByTripId($tripId = null)
    {
        return \DB::table('trips')
            ->join('trip_rows', 'trips.id', '=', 'trip_rows.trip_id')
            ->join('travelers', 'trip_rows.traveler_id', '=', 'travelers.id')
            ->where('trips.id', '=', $tripId)
            ->count();
    }

}