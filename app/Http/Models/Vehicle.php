<?php

namespace App\Http\Models;

/**
 * Class Vehicle
 */
class Vehicle extends \Eloquent
{

    public $timestamps = false;

    protected $fillable = [
        'name',
        'type',
        'registration_number',
        'capacity'
    ];

    protected $guarded = [];

        
}