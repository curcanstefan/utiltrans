<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/', 'TripsController@index');

    Route::get('trips', 'TripsController@get');
    Route::get('trips/{id}', 'TripsController@get');

    Route::get('trips/getByDay/{id}', 'TripsController@getByDay');
    Route::post('trips/set', 'TripsController@set');


    Route::get('driver', 'DriverController@get');
    Route::get('driver/{id}', 'DriverController@get');
    Route::post('driver', 'DriverController@set');


    Route::get('traveler', 'TravelerController@get');
    Route::get('traveler/{id}', 'TravelerController@get');
    Route::post('traveler', 'TravelerController@set');
    Route::put('traveler', 'TravelerController@set');
    Route::delete('traveler', 'TravelerController@delete');
    Route::post('traveler/lock', 'TravelerController@lock');
    Route::post('traveler/move', 'TravelerController@move');


    Route::get('route/', 'RouteController@get');
    Route::get('route/{id}', 'RouteController@get');


    Route::get('vehicle/{id}', 'VehicleController@get');
    Route::get('vehicle/', 'VehicleController@get');
    Route::post('vehicle', 'VehicleController@set');


    Route::get('env/date', 'EnvController@date');
    Route::get('env/pwd', 'EnvController@pwd');
    Route::get('migrate/status', 'EnvController@migrateStatus');
    Route::get('migrate', 'EnvController@migrate');
});
