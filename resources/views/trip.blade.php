@extends('layout_logged')

@section('content')
        <div class="container no-print">
            <div class="well margin-top">
                <h1 id="selectedDate">
                    Choose a day
                </h1>
                <div id="date" class="panel-body">
                </div>
                <div id="datePopoverContainer"></div>
            </div>

            <div class="well">
                <h1 id="tripDetailsTitle">
                    Trip Details
                </h1>
                <div id="tripDetails"></div>
            </div>
            <div class="bigSpacer"></div>
        </div>
        <div style="display:none;" id="includedContent">
            @include('templates')
        </div>
    <div class="print-area only-print"></div>
@endsection()
