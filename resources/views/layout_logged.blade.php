@extends('layouts.app')

@section('styles')
    <!--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/yeti/bootstrap.min.css">-->
    <!--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" integrity="sha384-y3tfxAZXuh4HwSYylfB+J125MxIs6mR5FOHamPBG064zB+AFeWH94NdvaCBm8qnd" crossorigin="anonymous">-->

    <link href="{!! asset('css/bootstrap-datepicker3.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/custom.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/ring.css') !!}" rel="stylesheet">

    <link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet">
@endsection


@section('scripts')
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <!--        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.1.1/js/tether.min.js" ></script>-->
    <!--        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>-->

    <script src="/js/bootstrap-datepicker.js"></script>
    <script src="/js/bootstrap-datepicker.ro.min.js"></script>

    <!--        <script src="/assets/js/material.min.js"></script>-->

    <script src="/js/trip-schedule.js"></script>
    <script src="/js/dataManager.js"></script>
    <script src="/js/dictionary.js"></script>
    <script src="/js/templates.js"></script>
    <script src="/js/mainApp.js"></script>
@endsection