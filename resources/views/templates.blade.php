@if (Auth::user()->is('admin'))
    <div class="xtemplate" id="editTrip">
        <form class="form-horizontal" id="edit_trip" name="edit_trip">
            <fieldset>
                <input type="hidden" id="id" name="id">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="title" class="col-md-3 control-label">{%trans%}new trip title label{%end trans%}</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" id="title" name="title" placeholder="{%trans%}new trip title placeholder{%end trans%}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="route" class="col-md-3 control-label">{%trans%}route label{%end trans%}</label>
                    <div class="col-md-7">
                        <select class="form-control" id="route" name="route">
                            <option value="04:00">04:00</option>
                            <option value="05:30">05:30</option>
                            <option value="09:00">09:00</option>
                            <option value="15:00">15:00</option>
                            <option value="22:00">22:00</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="start_date" class="col-md-3 control-label">{%trans%}new trip date label{%end trans%}</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" id="start_date" name="start_date" placeholder="{%trans%}new trip date placeholder{%end trans%}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="start_time" class="col-md-3 control-label">{%trans%}new trip time label{%end trans%}</label>
                    <div class="col-md-7">
                        <select class="form-control" id="start_time" name="start_time">
                            @for ($i = 0; $i < 24; $i++)
                                <option value="{{ substr('0' . $i, -2) }}:00">{{ substr('0' . $i, -2) }}:00</option>
                                <option value="{{ substr('0' . $i, -2) }}:30">{{ substr('0' . $i, -2) }}:30</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="vehicle" class="col-md-3 control-label">{%trans%}new trip vehicle label{%end trans%}</label>
                    <div class="col-md-7">
                        <select class="form-control" id="vehicle" name="vehicle">

                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="driver" class="col-md-3 control-label">{%trans%}driver label{%end trans%}</label>
                    <div class="col-md-7">
                        <select class="form-control" id="driver" name="driver">
                            <option value="04:00">04:00</option>
                            <option value="05:30">05:30</option>
                            <option value="09:00">09:00</option>
                            <option value="15:00">15:00</option>
                            <option value="22:00">22:00</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="title" class="col-md-3 control-label">{%trans%}trip price{%end trans%}</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" id="price" name="price" value="0">
                    </div>
                </div>
                <hr/>
                <div class="form-group well-material-grey-300">
                    <div class="col-md-4">
                        <span style="display: inline-block; padding-top: 0px;" class="checkbox">
                            <input type="checkbox" id="save_bulk" name="save_bulk">
                            <span class="checkbox-material">
                                <span class="check"></span>
                            </span>
                        </span>
                        <label for="save_bulk" class="control-label">{%trans%}save bulk trip{%end trans%}</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" id="bulk_end_date" name="bulk_end_date" placeholder="{%trans%}save bulk trip until date placeholder{%end trans%}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-7 col-md-offset-3">
                        <button type="button" class="btn btn-default cancel-trip">
                            {%trans%}cancel{%end trans%}
                            <div class="ripple-container"></div>
                        </button>
                        <button type="button" class="btn btn-primary save-trip">{%trans%}submit new{%end trans%}</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>


    <div class="xtemplate" id="displayVehicles">
        <ul class="list-group padding">
            <li class="list-group-item">
                <div class="row padding-sm">
                    <span class="col-sm-5">{%trans%}traveler name{%end trans%}</span>
                    <span class="col-sm-2">{%trans%}registration number{%end trans%}</span>
                    <span class="col-sm-2">{%trans%}capacity{%end trans%}</span>
                    <span class="col-sm-3"></span>
                </div>
            </li>
            {%each vehicles%}
            <hr>
            <li class="list-group-item">
                <div class="row padding-sm">
                    <span class="col-sm-5">{%name%}</span>
                    <span class="col-sm-2">{%registration_number%}</span>
                    <span class="col-sm-2">{%capacity%}</span>
                    <a href="#editVehicle/{%id%}" class=" col-sm-2 btn btn-success btn-flat btn-inline">{%trans%}edit{%end trans%}</a>
                </div>
            </li>
            {%end each%}
        </ul>
        <a href="#editVehicle" class="btn btn-primary btn-flat">+</a>
    </div>


    <div class="xtemplate" id="editVehicle">
        <form class="form-horizontal" id="edit_vehicle" name="edit_vehicle">
            <fieldset>
                <input type="hidden" id="id" name="id">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="title" class="col-md-3 control-label">{%trans%}new vehicle name label{%end trans%}</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="title" class="col-md-3 control-label">{%trans%}registration number{%end trans%}</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" id="registration_number" name="registration_number">
                    </div>
                </div>
                <div class="form-group">
                    <label for="title" class="col-md-3 control-label">{%trans%}capacity{%end trans%}</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" id="capacity" name="capacity">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-7 col-md-offset-3">
                        <button type="button" class="btn btn-default cancel-vehicle">
                            {%trans%}cancel{%end trans%}
                            <div class="ripple-container"></div>
                        </button>
                        <button type="button" class="btn btn-primary save-vehicle">{%trans%}submit new{%end trans%}</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>


    <div class="xtemplate" id="displayDrivers">
        <ul class="list-group padding">
            <li class="list-group-item">
                <div class="row padding-sm">
                    <span class="col-sm-5">{%trans%}traveler name{%end trans%}</span>
                    <span class="col-sm-3">{%trans%}traveler phone{%end trans%}</span>
                    <span class="col-sm-2"></span>
                    <span class="col-sm-2"></span>
                </div>
            </li>
            {%each drivers%}
            <hr>
            <li class="list-group-item">
                <div class="row padding-sm">
                    <span class="col-sm-5">{%name%}</span>
                    <span class="col-sm-3">{%phone%}</span>
                    <span class="col-sm-2"></span>
                    <a href="#editDriver/{%id%}" class=" col-sm-2 btn btn-success btn-flat btn-inline">{%trans%}edit{%end trans%}</a>
                </div>
            </li>
            {%end each%}
        </ul>
        <a href="#editDriver" class="btn btn-primary btn-flat">+</a>
    </div>


    <div class="xtemplate" id="editDriver">
        <form class="form-horizontal" id="edit_vehicle" name="edit_vehicle">
            <fieldset>
                <input type="hidden" id="id" name="id">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="title" class="col-md-3 control-label">{%trans%}traveler name{%end trans%}</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="title" class="col-md-3 control-label">{%trans%}traveler phone{%end trans%}</label>
                    <div class="col-md-7">
                        <input type="text" class="form-control" id="phone" name="phone">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-7 col-md-offset-3">
                        <button type="button" class="btn btn-default cancel-driver">
                            {%trans%}cancel{%end trans%}
                            <div class="ripple-container"></div>
                        </button>
                        <button type="button" class="btn btn-primary save-driver">{%trans%}submit new{%end trans%}</button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
@endif


<div class="xtemplate" id="editTripVehicleOption">
    {%each elements%}<option value="{%id%}">{%name%}</option>{%end each%}
</div>


@if (Auth::user()->is('user'))
<div class="xtemplate" id="addTravelerToTrip">
    <form class="form-horizontal" id="add_traveler" name="add_traveler">
        <fieldset>
            <input type="hidden" id="traveler_id" name="traveler_id">
            <input type="hidden" id="trip_id" name="trip_id">
            <input type="hidden" id="_method" name="_method" value="{%formMethod%}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group">
                <label for="traveler_name" class="col-md-3 control-label">{%trans%}traveler name{%end trans%}</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" id="traveler_name" name="traveler_name" placeholder="{%trans%}traveler name{%end trans%}">
                </div>
            </div>
            <div class="form-group">
                <label for="phone" class="col-md-3 control-label">{%trans%}traveler phone{%end trans%}</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="{%trans%}traveler phone{%end trans%}">
                </div>
            </div>
            <div class="form-group">
                <label for="seats" class="col-md-3 control-label">{%trans%}traveler seats{%end trans%}</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" id="seats" name="seats" placeholder="{%trans%}traveler seats{%end trans%}">
                </div>
            </div>
            <div class="form-group">
                <label for="departure_city" class="col-md-3 control-label">{%trans%}traveler departure{%end trans%}</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" id="departure_city" name="departure_city" placeholder="{%trans%}traveler departure{%end trans%}">
                </div>
            </div>
            <div class="form-group">
                <label for="destination_city" class="col-md-3 control-label">{%trans%}traveler destination{%end trans%}</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" id="destination_city" name="destination_city" placeholder="{%trans%}traveler destination{%end trans%}">
                </div>
            </div>
            <div class="form-group">
                <label for="total_price" class="col-md-3 control-label">{%trans%}traveler total price{%end trans%}</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" id="total_price" name="total_price" placeholder="{%trans%}traveler total price{%end trans%}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label for="amount_paid" class="col-md-3 control-label">{%trans%}traveler paid{%end trans%}</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" id="amount_paid" name="amount_paid" placeholder="{%trans%}traveler paid{%end trans%}">
                </div>
            </div>
            <div class="form-group">
                <label for="total_price" class="col-md-3 control-label">{%trans%}traveler plane connection{%end trans%}</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" id="plane_connection" name="plane_connection" placeholder="{%trans%}traveler plane connection{%end trans%}">
                </div>
            </div>
            <div class="form-group">
                <label for="amount_paid" class="col-md-3 control-label">{%trans%}traveler plane time{%end trans%}</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" id="plane_time" name="plane_time" placeholder="{%trans%}traveler plane time{%end trans%}">
                </div>
            </div>
            <div class="form-group">
                <label for="amount_paid" class="col-md-3 control-label">{%trans%}traveler details{%end trans%}</label>
                <div class="col-md-7">
                    <textarea type="text" class="form-control" id="details" name="details" placeholder="{%trans%}traveler details{%end trans%}" rows="3"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-7 col-md-offset-3">
                    <a href="#trip/{%tripId%}" type="button" class="btn btn-default cancel-traveler">
                        {%trans%}cancel{%end trans%}
                        <div class="ripple-container"></div>
                    </a>
                    <button type="button" class="btn btn-primary save-traveler">{%trans%}submit add traveler{%end trans%}</button>
                </div>
            </div>
        </fieldset>
    </form>
</div>
@endif


<div class="xtemplate" id="displayTrip">
    <div class="row well-material-blue-50 padding">
        <div class="col-sm-10">
            <h4 class="text-muted no-print">{%trans%}travelers{%end trans%}</h4>
        </div>
        <div class="col-sm-2">
            <a href="{%actionAddTraveler%}" class="btn btn-primary btn-flat save-trip {%activeClass%}">+</a>
        </div>
        <div id="addTraveler" style="clear:both;"></div>
        <div class="only-print print-header">
            <span class="col-sm-4 print-header-name">
                <h4 class="col-xs-2">{%trans%}travelers count{%end trans%}</h4>
                <h4 class="col-xs-5">{%trans%}traveler name{%end trans%}</h4>
                <h4 class="col-xs-5">{%trans%}traveler phone{%end trans%}</h4>
            </span>
            <span class="col-sm-4 print-header-paid">
                <h4 class="col-xs-4">{%trans%}ticket value{%end trans%}</h4>
                <h4 class="col-xs-4">{%trans%}traveler paid{%end trans%}</h4>
                <h4 class="col-xs-4">{%trans%}ticket remaining{%end trans%}</h4>
            </span>
            <span class="col-sm-4 print-header-plane">
                <h4 class="col-xs-6">{%trans%}plane connection{%end trans%}</h4>
                <h4 class="col-xs-6">{%trans%}traveler details{%end trans%}</h4>
            </span>
        </div>
            {%each travelers%}
            <h4 class="panel panel-{%reputationColor%}" aria-level="traveler" aria-selected="false" data-traveler-id="{%id%}">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-sm-6 print-name">
                            <span class="col-xs-2 text-muted">
                                <span class="badge">{%seats%}</span>
                                {%trans%}seats{%end trans%}
                            </span>
                            <span class="col-xs-5 text-muted">{%name%}</span>
                            <span class="col-xs-5">{%phone%}</span>
                        </div>
                        <div class="col-sm-6 print-paid">
                            <div class="col-xs-4">
                                <span class="label label-default">{%total_price%}</span>
                                <span class="no-print">{%trans%}ron{%end trans%}</span>
                            </div>
                            <div class="col-xs-4">
                                <span class="label label-default">{%paid%}</span>
                                <span class="no-print">{%trans%}paid{%end trans%}</span>
                            </div>
                            <div class="col-xs-4">
                                <span class="label label-{%paidColor%}">{%remaining%}</span>
                                <span class="no-print">{%trans%}remaining{%end trans%}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 print-plane">
                            <span class="col-xs-6">{%plane_connection%}</span>
                            <span class="col-xs-6">{%plane_time%}</span>
                        </div>
                        <div class="col-sm-6 print-details">
                            <span class="col-xs-6 text-muted">{%from_city%} - {%to_city%}</span>
                            <span class="col-xs-6 text-muted">{%details%}</span>
                        </div>
                    </div>
                    <hr class="no-print">
                    <div class="row no-print">
                        <div class="col-sm-5">
                            @if (Auth::user()->is('admin'))
                                <span class="checkbox checkbox-info">
                                    <label>
                                        <input type="checkbox" aria-level="select-traveler" data-traveler-id="{%id%}">
                                        <span class="checkbox-material">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </span>
                                <div style="margin: 0px;" class="btn-group" role="group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {%trans%}admin traveler options{%end trans%}
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <button class="btn btn-flat btn-warning btn-sm btn-inline pull-right" onclick="schedule.initUnlockTraveler({%trip_id%},{%id%});">
                                                <i class="glyphicon glyphicon-unlock"></i>
                                                {%trans%}unlock{%end trans%}
                                            </button>
                                        </li>
                                        <li>
                                            <button class="btn btn-flat btn-primary btn-sm btn-inline pull-right" onclick="schedule.initLockTraveler({%trip_id%},{%id%});">
                                                <i class="glyphicon glyphicon-lock"></i>
                                                {%trans%}lock{%end trans%}
                                            </button>
                                        </li>
                                        <li>
                                            <a class="btn btn-flat btn-success btn-sm btn-inline pull-right" href="#editTraveler/{%trip_id%}/{%id%}">
                                                <i class="glyphicon glyphicon-pencil"></i>
                                                {%trans%}edit{%end trans%}
                                            </a>
                                        </li>
                                        <li>
                                            <button class="btn btn-flat btn-danger btn-sm btn-inline pull-right" onclick="schedule.deleteTraveler({%trip_id%},{%id%});">
                                                <i class="glyphicon glyphicon-trash"></i>
                                                {%trans%}delete{%end trans%}
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            @endif
                        </div>
                        <div class="col-sm-7 text-muted">
                            @if (Auth::user()->is('admin'))
                                <i class="glyphicon glyphicon-user"></i>
                                {%added_by%}
                            @endif
                            <button class="btn btn-flat btn-primary btn-sm btn-inline pull-right" onclick="schedule.{%stateAction%}Traveler({%trip_id%},{%id%});">
                                <i class="glyphicon glyphicon-{%stateActionIcon%}"></i>
                                {%stateActionDisplay%}
                            </button>
                            <a class="btn btn-flat btn-success btn-sm btn-inline pull-right {%classEdit%} {%tripActiveClass%}" href="#editTraveler/{%trip_id%}/{%id%}">
                                <i class="glyphicon glyphicon-pencil"></i>
                                {%trans%}edit{%end trans%}
                            </a>
                            <button class="btn btn-flat btn-danger btn-sm btn-inline pull-right {%classEdit%} {%tripActiveClass%}" onclick="schedule.deleteTraveler({%trip_id%},{%id%});">
                                <i class="glyphicon glyphicon-trash"></i>
                                {%trans%}delete{%end trans%}
                            </button>
                        </div>
                    </div>
                </div>
            </h4>
            {%end each%}

        <div aria-hidden="true" style="display: none;" id="move-travelers-panel" class="panel panel-body traveler-selected">
            <span class="col-sm-2">
                <input type="text" id="move-travelers-date" name="move-travelers-date" placeholder="{%trans%}new trip date placeholder{%end trans%}" class="form-control">
            </span>
            <span class="col-sm-3">
                <select id="move-travelers-select" class="form-control"></select>
            </span>
            <button class="btn btn-info btn-inline" id="move-travelers-btn" onclick="schedule.initMoveTravelers();">
                {%trans%}move travelers{%end trans%}
            </button>
        </div>

        <h4 class="panel panel-primary">
            <div class="panel-heading print-total">
                <div class="row">
                    <span class="col-sm-6">{%trans%}total{%end trans%}</span>
                    <span class="col-sm-6">
                        <span class="col-xs-4">
                            <span class="label">{%totalAmount%}</span>
                            {%trans%}ron{%end trans%}
                        </span>
                        <span class="col-sm-4">
                            <span class="label">{%totalPaid%}</span>
                            {%trans%}paid{%end trans%}
                        </span>
                        <span class="col-sm-4">
                            <span class="label">{%totalRemaining%}</span>
                            {%trans%}remaining{%end trans%}
                        </span>
                    </span>
                </div>
            </div>
        </h4>
    </div>

    <h4 class="row well-material-blue-50">
        <ul class="list-group padding">
            <li class="list-group-item">
                <div class="row padding-sm">
                    <span class="col-sm-4">{%trans%}route label{%end trans%}</span><span class="col-sm-8">{%title%}</span>
                </div>
            </li>
            <li class="list-group-item">
                <div class="row padding-sm">
                    <span class="col-sm-4">{%trans%}departure time{%end trans%}</span><span class="col-sm-8">{%displayStartDate%}</span>
                </div>
            </li>
            <li class="list-group-item">
                <div class="row padding-sm">
                    <span class="col-sm-4">{%trans%}arrival time{%end trans%}</span><span class="col-sm-8">{%displayEndDate%}</span>
                </div>
            </li>
            <li class="list-group-item">
                <div class="row padding-sm">
                    <span class="col-sm-4">{%trans%}trip price{%end trans%}</span><span class="col-sm-8">{%price%}</span>
                </div>
            </li>
            @if (Auth::user()->is('admin'))
            <li class="list-group-item">
                <div class="row padding-sm">
                    <span class="col-sm-4">{%trans%}vehicle{%end trans%}</span><span class="col-sm-8">{%vehicleName%}</span>
                </div>
            </li>
            <li class="list-group-item">
                <div class="row padding-sm">
                    <span class="col-sm-4">{%trans%}driver label{%end trans%}</span><span class="col-sm-8">{%driverName%}</span>
                </div>
            </li>
            @endif
            <li class="list-group-item">
                <div class="row padding-sm">
                    <span class="col-sm-4">{%trans%}occupied seats{%end trans%}</span><span class="col-sm-8">{%occupiedSeats%}</span>
                </div>
            </li>
        </ul>
    </h4>
    @if (Auth::user()->is('admin'))
        <a href="#editTrip/{%id%}" class="btn btn-success save-trip btn-flat">{%trans%}edit{%end trans%}</a>
        <button onclick="schedule.printTrip()" class="btn btn-info btn-flat pull-right">{%trans%}print{%end trans%}</button>
    @endif
</div>


<script type="xtemplate" id="printTrip">
    <table>
        <tr>
            <td>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th></th>
                        <th colspan="3">DATA: &nbsp; {%printStartDate%} &nbsp;&nbsp;&nbsp; ORA: &nbsp; {%printStartTime%}</th>
                        <th colspan="3">{%routeName%}</th>
                        <th>SOFER: &nbsp; {%driverName%}</th>
                    </tr>
                    <tr>
                        <th>NR.</th>
                        <th>LOC</th>
                        <th>NUME SI PRENUME</th>
                        <th>TELEFON</th>
                        <th>TARIF</th>
                        <th>ACHITAT</th>
                        <th>REST</th>
                        <th>DETALII ZBOR / OBSERVATII</th>
                    </tr>
                    </thead>
                    <tbody>
                    {%each travelers%}
                    <tr>
                        <td align="center">{%row_id%}</td>
                        <td align="center"><b>{%seats%}</b></td>
                        <td>{%name%}</td>
                        <td>{%phone%}</td>
                        <td align="right">{%total_price%}</td>
                        <td align="right">{%paid%}</td>
                        <td align="right">{%remaining%}</td>
                        <td>{%plane_connection%} {%plane_time%} - {%from_city%} {%to_city%}</td>
                    </tr>
                    {%end each%}
                    </tbody>
                </table>
            </td>
            <td>
                <table class="table table-bordered">
                    <thead>
                    <tr><th>DETALII</th></tr>
                    <tr><th>MASINA</th></tr>
                    </thead>
                    <tbody>
                        <tr><td>{%vehicleName%}</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>AVANS</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>INCASARI</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>CHELTUIELI</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td>REST</td></tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
</script>


<div class="xtemplate" id="listTripsForDay">
    {%each errors%}
        <div class="padding">{%message%}</div>
    {%end each%}

    {%each trips%}
        <a href="#trip/{%id%}">
            <div class="well well-sm {%classLocked%}">
                <div class="row">
                    <span class="col-sm-3 text-info">
                        {%displayStartDate%}
                    </span>
                    <span class="col-sm-6">
                        {%title%}
                    </span>
                    <span class="col-sm-2 text-success">
                        {%occupiedSeats%}
                    </span>
                </div>
            </div>
        </a>
    {%end each%}

    @if (Auth::user()->is('admin'))
    {%each newTrips%}
        <a href="#editTrip/newTrip/{%date%}">
            <div class="well well-sm">
                <div class="row">
                    <span class="col-sm-12">
                        + {%title%}
                    </span>
                </div>
            </div>
        </a>
    {%end each%}
    @endif
</div>


<div class="xtemplate" id="printTraveler">
    <style media="print">
        #ticket table {
            border-collapse: collapse;
            width: 100%;
            font-family: Arial;
        }

        #ticket table, td, th {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 5px 10px;
        }

        #ticket table table {
            border: transparent;
        }

        #ticket h2,
        #ticket h4 {
            font-weight: normal;
        }
    </style>
    <div id="ticket">
        <table>
            <tr>
                <td><h2>{%traveler.added_by%}</h2></td>
                <td><h3><b>TICHET DE REZERVARE<br/>RESERVATION NOTE</b></h3></td>
                <td><h4>Tel agentie</h4></td>
            </tr>
            <tr>
                <td>Data emiterii</td>
                <td>{%trip.displayStartDate%}</td>
                <td>Semnatura de primire</td>
            </tr>
            <tr>
                <td><b>NUME</b></td>
                <td><b>{%traveler.name%}</b></td>
                <td rowspan="9"></td>
            </tr>
            <tr>
                <td><b>TEL</b></td>
                <td><b>{%traveler.phone%}</b></td>
            </tr>
            <tr>
                <td>De la</td>
                <td>{%traveler.from_city%}</td>
            </tr>
            <tr>
                <td>Pana la</td>
                <td>{%traveler.to_city%}</td>
            </tr>
            <tr>
                <td>Data plecarii</td>
                <td>{%trip.displayStartDate%}</td>
            <tr>
                <td>Data sosirii</td>
                <td>{%trip.displayEndDate%}</td>
            </tr>
            <tr>
                <td>TARIF DE CALATORIE</td>
                <td align="right">{%traveler.total_price%} LEI</td>
            </tr>
            <tr>
                <td>TAXA DE REZERVARE</td>
                <td align="right">0 LEI</td>
            </tr>
            <tr>
                <td>REST DE PLATA</td>
                <td align="right">{%traveler.remaining%} LEI</td>
            </tr>
        </table>
    </div>
</div>

<script type="xtemplate" id="templateModal">
    <div class="modal fade confirmationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">{%title%}</h4>
                </div>
                <div class="modal-body">
                    {%body%}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{%cancelText%}</button>
                    <button type="button" class="btn btn-primary modalConfirmBtn">{%confirmText%}</button>
                </div>
            </div>
        </div>
    </div>
</script>
