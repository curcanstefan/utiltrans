var dictionary = {
    availableLanguages: ['ro'],
    language: 'ro',
    setLanguage: function(id) {
        if(this.availableLanguages.indexOf(id) > -1) {
            this.language = id;
        }
    },
    get: function(id) {
        return dictionarySet[this.language][id] || id;
    }
};

var dictionarySet = {
    'ro': {
        'no trips for this day':        'Nu sunt inregistrari pe aceasta zi',

        'departure time':               'Pleaca la ',
        'arrival time':                 'Ajunge la ',
        'vehicle':                      'Transport cu ',
        'seats':                        ' ',
        'total seats':                  'Locuri in masina',
        'free seats':                   'Locuri libere',
        'occupied seats':               'Locuri ocupate',
        'travelers':                    'Calatori',
        'add new trip':                 'Adauga transport',
        'new trip title':               'Transport nou',
        'new trip title label':         'Titlu transport',
        'new trip title placeholder':   'titlu transport',
        'new trip date label':          'Ziua plecarii',
        'new trip date placeholder':    'Alege o zi',
        'new trip time label':          'Ora plecarii',
        'new trip vehicle label':       'Masina',
        'route label':                  'Ruta',
        'driver label':                 'Sofer',
        'trip price':                   'Pret',
        'submit':                       'Ok',
        'submit new':                   'Salveaza',
        'cancel':                       'Anuleaza',
        'edit':                         'Modifica',

        'traveler name':                'Nume',
        'traveler phone':               'Telefon',
        'traveler seats':               'Locuri rezervate',
        'traveler departure':           'Urca la',
        'traveler destination':         'Coboara la',
        'traveler total price':         'Pret total',
        'traveler paid':                'Achitat',
        'traveler plane connection':    'Conexiune avion',
        'traveler plane time':          'Ora decolare/aterizare',
        'traveler details':             'Detalii',
        'submit add traveler':          'Adauga',

        'paid':                         'platit',
        'remaining':                    'rest',
        'ron':                          'RON',

        'travelers count':              'Pers.',
        'ticket value':                 'Tarif',
        'ticket remaining':             'Rest',
        'plane connection':             'Avion',

        'lock':                         'Rezerva',
        'unlock':                       'Anuleaza rezervarea',
        'print':                        'Imprima',
        'trip locked':                  'Transportul este blocat. Nu mai puteti aduce modificari asupra lui. Va rugam utilizati un transport de actualitate.',
        'delete':                       'Sterge',

        'save bulk trip':               'Salveaza mai multe pana in data',
        'admin traveler options':       'Optiuni admin',

        'vehicles title':               'Masini',
        'new vehicle title':            'Adauga masina noua',
        'registration number':          'Numar de inmatriculare',
        'capacity':                     'Locuri',
        'new vehicle name label':       'Nume masina',

        'drivers title':                'Soferi',
        'new driver title':             'Adauga sofer',
        'available':                    'Disponibil',

        'move travelers':               'Muta calatorii',
    }
};
