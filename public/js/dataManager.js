var dataManager = {
    getTrips: function(id, callback) {
        var objectCallback = function(data) {
            var trips = schedule.newTripFromArray(data);
            if(typeof callback === 'function'){
                callback(trips);
            }
        };
        var key = 'trips' + (id || '');
        var cachedData = dataManager.getTripsFromCache(key, callback);
        if(cachedData !== 'undefined' && cachedData !== null){
            return objectCallback(cachedData);
        }

        return dataManager.getTripsFromServer(id, key, objectCallback);
    },
    getTripsFromCache: function(key) {
        return dataManager.getFromCache(key);
    },
    getTripsFromServer: function(id, key, callback) {
        dataManager.getFromServer(
            'trips/' + (id || ''),
            key,
            function(data) {
                schedule.addTripsToCache(data);
                if (typeof callback === 'function') {
                    callback(data);
                }
            });
    },
    getRoutes: function(id, callback) {
        dataManager.getFromServer(
            'route/' + (id || ''),
            'routes' + (id || ''),
            callback);
    },
    getTravelers: function(id, callback) {
        dataManager.getFromServer(
            'traveler/' + (id || ''),
            'travelers' + (id || ''),
            callback);
    },
    getFromCache: function(id) {
        return null;
        return JSON.parse(localStorage.getItem(id));
    },
    setToCache: function(key, data) {
        return false;
        return localStorage.setItem(key, JSON.stringify(data));
    },
    getFromServer: function(url, key, callback) {
        $.getJSON(url, function(data){
            dataManager.setToCache(key, data);
            if(typeof callback === 'function'){
                callback(data);
            }
        });
    },
    getTripsByDay: function(searchDate, callback) {
        /* global customTime */
        /* global schedule */
        var url = 'trips/getByDay/'+customTime.format(searchDate, 'serverDate');
        $.getJSON(url, function(data){
            schedule.addTripsToCache(data);
            if(typeof callback === 'function'){
                callback(data);
            }
        });
    },
    addTrip: function(input, callback) {
        // add to server
        // add to cache
        var objectCallback = function(data, status) {
            if(typeof callback === 'function'){
                callback(data);
            }
        };
        dataManager.addTripToServer(input, objectCallback);
    },
    addTripToServer: function(trip, callback) {
        dataManager.addToServer(
            'trips/set',
            trip,
            callback);
    },
    addTravelerToTrip: function(trip, callback) {
        dataManager.addToServer(
            'traveler',
            trip,
            callback);
    },
    lockTraveler: function(tripRow, callback) {
        dataManager.addToServer(
            'traveler/lock',
            tripRow,
            callback);
    },
    moveTravelers: function(data, callback) {
        dataManager.addToServer(
            'traveler/move',
            data,
            callback);
    },
    getVehicles: function(id, callback) {
        dataManager.getFromServer(
            'vehicle/' + (id || ''),
            'vehicles' + (id || ''),
            callback);
    },
    addVehicle: function(input, callback) {
        // add to server
        // add to cache
        var objectCallback = function(data, status) {
            if(typeof callback === 'function'){
                callback(data, status);
            }
        };
        dataManager.addVehicleToServer(input, objectCallback);
    },
    addVehicleToServer: function(vehicle, callback) {
        dataManager.addToServer(
            'vehicle',
            vehicle,
            callback);
    },
    getDrivers: function(id, callback) {
        dataManager.getFromServer(
            'driver/' + (id || ''),
            'drivers' + (id || ''),
            callback);
    },
    addDriver: function(input, callback) {
        // add to server
        // add to cache
        var objectCallback = function(data, status) {
            if(typeof callback === 'function'){
                callback(data, status);
            }
        };
        dataManager.addDriverToServer(input, objectCallback);
    },
    addDriverToServer: function(driver, callback) {
        dataManager.addToServer(
            'driver',
            driver,
            callback);
    },
    addToServer: function(url, data, callback) {
        var objectCallback = function(data, status){
            if(typeof callback === 'function'){
                callback(data, status);
            }
        };
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            complete: objectCallback
        });
    }
};
