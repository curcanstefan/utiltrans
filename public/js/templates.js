var templates = {
    render: function(template, data, row_id) {
        var result = template;
        // for each
        result = result.replace(
            /{%each (.*?)%}([^]+?){%end each%}/g,
            function(fullMatch, id, match) {
                return templates.each(match, data[id]);
            });

        // translate
        result = result.replace(/{%trans%}(.*?){%end trans%}/g, templates.translate);

        // values
        result = result.replace(/{%(.*?)%}/g, function(fullMatch, match) {
            if (match === 'row_id') {
                return row_id;
            }
            var keys = match.split('.');
            var resultData = data;
            for(var i=0; i<keys.length; i++) {
                resultData = resultData[keys[i]];
            }
            return resultData;
        });
        return result;
    },
    translate: function(fullMatch, match) {
        return dictionary.get(match);
    },
    each: function(template, data) {
        var result = [];
        $(data).each(function(id, value) {
            result.push(templates.render(template, value, id + 1));
        });
        return result.join('');
    }
};
