var customTime = {
    format: function(inputDate, format){
        var monthNames = ["IAN", "FEB", "MAR", "APR", "MAI", "IUN",
            "IUL", "AUG", "SEP", "OCT", "NOV", "DEC"
        ];
        var time = ('0' + inputDate.getHours()).slice(-2) + ':' + ('0' + inputDate.getMinutes()).slice(-2);
        var date = ('0' + inputDate.getDate()).slice(-2) + '-' + ('0' + (inputDate.getMonth()+1)).slice(-2) + '-' + inputDate.getFullYear();
        var serverDate = inputDate.getFullYear() + '-' + ('0' + (inputDate.getMonth()+1)).slice(-2) + '-' + ('0' + inputDate.getDate()).slice(-2);
        var humanDate = ('0' + inputDate.getDate()).slice(-2) + ' ' + monthNames[inputDate.getMonth()];
        switch(format){
            case 'time':
                return time;
            case 'date':
                return date;
            case 'serverDate':
                return serverDate;
            case 'humanDate':
                return humanDate;
            case 'datetime':
                return date + ' ' + time;
        }
    },
    dateFromString: function(stringDate) {
        var parts = stringDate.split(/ |-|:/);
        var year  = parseInt(parts[0]);
        var month = parseInt(parts[1])-1;
        var day   = parseInt(parts[2]);
        var hour  = parseInt(parts[3]);
        var min   = parseInt(parts[4]);
        var sec   = parseInt(parts[5]);
        return new Date(year, month, day, hour, min, sec);
    }
};

var scrollToDetails = function(){
    $('html, body').animate({
        scrollTop: $("#tripDetailsTitle").offset().top - 50
        }, 500);
};

$(document).ready(function(){
    schedule.initData();

    $(window).on('hashchange', function(){
        /* global valueSet */
        /* global dataManager */
        /* global schedule */
        var hash = (location.hash.replace( /^#/, '' ) || 'blank');
        if(!hash) return;
        valueSet    = hash.split('/');
        var action  = valueSet[0];
        var val1    = valueSet[1];
        var val2    = valueSet[2];

        var ensureTripIsDisplayed = function(tripId, callback) {
            if (currentTrip) {
                if (typeof callback === 'function') {
                    callback();
                }
            }
            dataManager.getTrips(tripId, function(data) {
                schedule.displayTripDetails(data);
                if (typeof callback === 'function') {
                    callback();
                }
            });
        };

        switch(action) {
            case 'trip':
                if (val1 == 'undefined' && currentTrip && currentTrip.id) location.hash = '#trip/' + currentTrip.id;
                $("#tripDetails").html('<div class="spinner"></div>');
                dataManager.getTrips(val1, schedule.displayTripDetails);
                scrollToDetails();
                break;
            case 'editTrip':
                if (parseInt(val1)) {
                    dataManager.getTrips(val1, function(data) {
                        schedule.editTrip(val1, data[0]);
                    });
                } else {
                    schedule.editTrip(val1, val2);
                }
                scrollToDetails();
                break;
            case 'addTraveler':
                ensureTripIsDisplayed(val1, function(){
                    schedule.editTraveler(val1);
                    $('html, body').animate({
                        scrollTop: $("#addTraveler").parent().offset().top - 50
                    }, 500);
                });
                break;
            case 'editTraveler':
                ensureTripIsDisplayed(val1, function(){
                    schedule.editTraveler(val1, val2);
                    $('html, body').animate({
                        scrollTop: $("#addTraveler").parent().offset().top - 50
                    }, 500);
                });
                break;
            case 'vehicles':
                $('#date').parent().hide();
                dataManager.getVehicles(null, schedule.displayVehicles);
                break;
            case 'editVehicle':
                $('#date').parent().hide();
                if (parseInt(val1)) {
                    dataManager.getVehicles(val1, function(data) {
                        schedule.editVehicle(val1, data[0]);
                    });
                } else {
                    schedule.editVehicle(val1);
                }
                scrollToDetails();
                break;
            case 'drivers':
                $('#date').parent().hide();
                dataManager.getDrivers(null, schedule.displayDrivers);
                break;
            case 'editDriver':
                $('#date').parent().hide();
                if (parseInt(val1)) {
                    dataManager.getDrivers(val1, function(data) {
                        schedule.editDriver(val1, data[0]);
                    });
                } else {
                    schedule.editDriver(val1);
                }
                scrollToDetails();
                break;
        }
    });

    }
);

var modal = {
    default: {
        title:            '',
        body:             '',
        cancelText:       'Anuleaza',
        confirmText:      'Confirma',
        closeOnSuccess:   true,
        showCloseButton:  true,
        showConfirmBtn:   true,
        confirmCallback:  null
    },

    show: function(options) {
        var data = $.extend({}, modal.default, options);
        var template = $('script#templateModal').html();
        $('body > .confirmationModal').remove();
        $('body').append(templates.render(template, data));

        var popup = $('body > .confirmationModal');
        popup.modal('show');
        if (!data.showConfirmBtn) {
            popup.find('.modalConfirmBtn').remove();
        }
        if (typeof data.confirmCallback == 'function') {
            popup.find('.modalConfirmBtn').click(function() {
                data.confirmCallback.call();
                popup.modal('hide');
                $('body > .confirmationModal').remove();
                $('body > .modal-backdrop').remove();
                $('body').removeClass('modal-open');
            });
        }
    }
};