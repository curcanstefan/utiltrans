var trips = [];
var currentTrip = null;

var schedule = {
    newTrip: function(
        id, title,
        startDate, endDate,
        routeId, routeName,
        driverId, driverName,
        vehicleId, vehicleName, vehicleCapacity,
        travelers, occupiedSeats, locked,
        price)
    {
        var trip = {};
        trip.id = id;
        trip.title = title;
        if (typeof startDate == 'object') {
            trip.startDate = startDate;
        }
        if (typeof startDate == 'string') {
            trip.startDate = customTime.dateFromString(startDate);
        }
        if (typeof endDate == 'object') {
            trip.endDate = endDate;
        }
        if (typeof endDate == 'string') {
            trip.endDate = customTime.dateFromString(endDate);
        }
        trip.routeId = routeId;
        trip.routeName = routeName;
        trip.driverId = driverId;
        trip.driverName = driverName;
        trip.vehicle_id = vehicleId;
        trip.vehicleName = vehicleName;
        trip.vehicleCapacity = vehicleCapacity;
        trip.occupiedSeats = occupiedSeats;
        trip.locked = locked;
        trip.price = parseInt(price);
        if(travelers instanceof Array) trip.travelers = travelers;
        else if(travelers instanceof Object) trip.travelers = schedule.newTraveler(travelers);

        return trip;
    },
    newTripFromObject: function(trip){
        return schedule.newTrip(
            trip.id,
            trip.title,
            trip.start_date,
            trip.end_date,
            trip.route_id,
            trip.route_name,
            trip.driver_id,
            trip.driver_name,
            trip.vehicle_id,
            trip.vehicle_name,
            trip.vehicle_capacity,
            trip.travelers,
            trip.travelers_count,
            trip.locked,
            trip.price
        );
    },
    newTripFromArray: function(trips){
        var result = [];
        $(trips).each(function(id, trip) {
            result.push(
                schedule.newTripFromObject(trip)
            );
        });
        return result;
    },
    addTripsToCache: function(newTrips) {
        $(newTrips).each(function(key, trip) {
            trips[trip.id] = schedule.newTripFromObject(trip);
        });
    },
    newVehicle: function(id, name, registration_number, capacity)
    {
        var vehicle = {};
        vehicle.id = id;
        vehicle.name = name;
        vehicle.registration_number = registration_number;
        vehicle.capacity = capacity;

        return vehicle;
    },
    newDriver: function(id, name, phone, active)
    {
        var driver = {};
        driver.id = id;
        driver.name = name;
        driver.phone = phone;
        driver.active = active;

        return driver;
    },
    initData: function(){
        schedule.getTripsByDate(new Date());
        /*dataManager.getTrips(null, function(data){
            trips = trips.concat(data);
        });*/
    },
    newTraveler: function(input){
        return {
            id:         input['id'],
            name:       input['name'],
            phone:      input['phone'],
            reputation: input['reputation'],
            totalPrice: input['total_price'],
            paid:       input['paid'],
            seats:      input['seats'],
            fromCity:   input['from_city'],
            toCity:     input['to_city'],
            connection: input['plane_connection'],
            planeTime:  input['plane_time'],
            details:    input['details'],
            addedBy:    input['added_by'],
        };
    },
    getTripsByDate: function(searchDate){
        if(!(searchDate instanceof Date)) return [];
        return trips.filter(function(el){
            return (el.startDate.getDate() == searchDate.getDate()
            && el.startDate.getMonth() == searchDate.getMonth()
            && el.startDate.getFullYear() == searchDate.getFullYear());
            });
    },
    getTripById: function(id) {
        var result = trips.filter(function(el){
            return (el.id == id);
            });
        return result.length ? result[0] : null;
    },
    displayTripDetails: function(trip) {
        trip = trip[0];
        /*global currentTrip*/
        currentTrip = trip;
        var titleContainer = $("#tripDetailsTitle");
        var container = $("#tripDetails");
        titleContainer.html(trip.title);

        var totalAmount = 0;
        var totalPaid = 0;

        $(trip.travelers).each(function(id, traveler) {
            traveler.trip_id = trip.id;
            traveler.reputationColor = 'default';
            if(traveler.reputation < 0) traveler.reputationColor = 'danger';
            if(traveler.reputation > 0) traveler.reputationColor = 'success';
            traveler.paidColor = 'warning';
            traveler.remaining = traveler.total_price - traveler.paid;
            if(traveler.paid == traveler.total_price) traveler.paidColor = 'success';
            if (parseInt(traveler.locked)) {
                traveler.stateAction = 'print';
                traveler.stateActionIcon = 'print';
                traveler.classEdit = 'hidden';
                traveler.stateActionDisplay = dictionary.get('print');
            } else {
                traveler.stateAction = 'initLock';
                traveler.stateActionIcon = 'lock';
                traveler.stateActionDisplay = dictionary.get('lock');
                traveler.classEdit = '';
            }
            traveler.tripActiveClass = trip.locked ? 'hidden' : '';
            totalAmount += parseFloat(traveler.total_price);
            totalPaid += parseFloat(traveler.paid);
        });

        trip.actionAddTraveler = trip.locked ? '' : '#addTraveler/' + trip.id;
        trip.activeClass = trip.locked ? 'hidden' : '';
        trip.totalAmount = totalAmount;
        trip.totalPaid = totalPaid;
        trip.totalRemaining = totalAmount - totalPaid;
        trip.displayStartDate = customTime.format(trip.startDate, 'datetime');
        trip.printStartDate = customTime.format(trip.startDate, 'humanDate');
        trip.printStartTime = customTime.format(trip.startDate, 'time');
        trip.displayEndDate = customTime.format(trip.endDate, 'datetime');

        var tripTemplate = $('.xtemplate#displayTrip').html();
        var tripDetails = templates.render(tripTemplate, trip);

        container.html(tripDetails);

        $('input[aria-level="select-traveler"]').change(function(event) {
            var selected = event.target.checked;
            var parent = $(this).closest('.panel');
            parent.attr('aria-selected', selected);
            parent.toggleClass('traveler-selected', selected);
            parent.toggleClass('panel-info', selected);

            var selectedTravelersCount = $('input[aria-level="select-traveler"]:checked').length;
            var dateSelected = function(e){
                var callback = function(data) {
                    var template = '{%each trips%}<option value="{%id%}">{%title%}</option>{%end each%}';
                    var options = templates.render(template, {'trips': data});
                    $('#move-travelers-select').html(options);
                };
                dataManager.getTripsByDay(e.date, callback);
            };

            if (selectedTravelersCount == 1 && $('#move-travelers-panel').is(':hidden')) {
                $('#move-travelers-date').datepicker({
                        startDate: "01/01/2015",
                        weekStart: 1,
                        todayBtn: "linked",
                        language: "ro",
                        todayHighlight: true
                    })
                    .off('changeDate')
                    .on('changeDate', dateSelected)
                    .datepicker('setDate', trip.startDate);
            }

            // show move area if any travelers checked
            $('#move-travelers-panel').toggle(selectedTravelersCount > 0);
        });
        
        $('#datePopoverContainer > div').remove();
    },
    printTrip: function() {
        var template = $('script#printTrip').html();
        var container = $("body .print-area");
        container.html(templates.render(template, currentTrip));
        window.print();
    },
    editTrip: function(id, date) {
        /* global templates */
        var trip = schedule.newTrip();
        var newTrip = true;

        if (parseInt(id)) {
            trip = date;
            newTrip = false;
        }

        var template = $('.xtemplate#editTrip').html();
        var titleContainer = $("#tripDetailsTitle");
        var container = $("#tripDetails");
        titleContainer.html(trip.title || dictionary.get('new trip title'));
        container.html(templates.render(template));

        // get vehicles and populate select options
        var optionTemplate = $('.xtemplate#editTripVehicleOption').html();
        dataManager.getVehicles(null, function(data) {
            var optionContainer = container.find('select#vehicle');
            var options = templates.render(
                optionTemplate,
                {'elements': data}
            );
            optionContainer.html(options);

            // set stored title
            container.find('#title').val(trip.title);
            // set stored vehicle
            if (trip.vehicle_id) {
                optionContainer.val(trip.vehicle_id);
            }
        });

        // get routes and populate select options
        dataManager.getRoutes(null, function(data) {
            var optionContainer = container.find('select#route');
            var options = templates.render(
                optionTemplate,
                {'elements': data}
            );
            optionContainer.html(options);

            // set stored vehicle
            if (trip.route_id) {
                optionContainer.val(trip.route_id);
            }
        });

        // get drivers and populate select options
        dataManager.getDrivers(null, function(data) {
            var optionContainer = container.find('select#driver');
            var options = templates.render(
                optionTemplate,
                {'elements': data}
            );
            optionContainer.html(options);

            // set stored vehicle
            if (trip.driver_id) {
                optionContainer.val(trip.driver_id);
            }
        });

        var datepickerSelector  = $('#edit_trip #start_date');
        var bulkSaveStart       = $('#edit_trip #bulk_start_date');
        var bulkSaveEnd         = $('#edit_trip #bulk_end_date');
        datepickerSelector.datepicker({
            format: "dd-mm-yyyy",
            startDate: "01-01-2015",
            weekStart: 1,
            todayBtn: "linked",
            language: "ro",
            todayHighlight: true/*,
            datesDisabled: ['06/06/2015', '06/21/2015']*/
        });
        bulkSaveStart.datepicker({
            format: "dd-mm-yyyy",
            startDate: "01-01-2015",
            weekStart: 1,
            todayBtn: "linked",
            language: "ro",
            todayHighlight: true/*,
             datesDisabled: ['06/06/2015', '06/21/2015']*/
        });
        bulkSaveEnd.datepicker({
            format: "dd-mm-yyyy",
            startDate: "01-01-2015",
            weekStart: 1,
            todayBtn: "linked",
            language: "ro",
            todayHighlight: true/*,
             datesDisabled: ['06/06/2015', '06/21/2015']*/
        });
        // set stored date
        if (!newTrip) {
            datepickerSelector.datepicker('setDate', customTime.format(trip.startDate, 'date'));
            container.find('#start_time').val(customTime.format(trip.startDate, 'time'));
            container.find('#id').val(trip.id);
            container.find('#price').val(trip.price);
            bulkSaveStart.datepicker('setDate', customTime.format(trip.startDate, 'date'));
            bulkSaveEnd.datepicker('setDate', customTime.format(trip.startDate, 'date'));
        } else if (date) {
            datepickerSelector.datepicker('setDate', date);
            bulkSaveStart.datepicker('setDate', date);
            bulkSaveEnd.datepicker('setDate', date);
        }
        // add functionality to save button
        container.find('.save-trip').on('click',
            function(event) {
                /* global dataManager */
                var formValues = container.find('form').serialize();
                // save trip to db
                dataManager.addTrip(formValues, function(data) {
                    var tripId = newTrip ? data.responseText : trip.id;
                    location.hash = '#trip/' + tripId;
                });
                // add trip to js store
            }
        );
        // add functionality to save button
        container.find('.cancel-trip').on('click',
            function(event) {
                location.hash = '#trip/' + trip.id;
            }
        );
    },

    editTraveler: function(tripId, travelerId) {
        var isNewTraveler = true;
        if (travelerId && travelerId > 0) {
            isNewTraveler = false;
            var traveler = $.grep(currentTrip.travelers, function(n,i) {return n['id'] == travelerId;})[0];
        }
        var template = $('.xtemplate#addTravelerToTrip').html();
        var container = $("#tripDetails #addTraveler");
        var dataForTemplate = {
            'tripId': tripId,
            'formMethod': isNewTraveler ? 'POST' : 'PUT'
        };
        container.html(templates.render(template, dataForTemplate));

        container.addClass('well');

        // get drivers and populate select options
        dataManager.getTravelers(null, function(data) {
            var travelersNames = [];
            var travelersPhones = [];
            $(data).each(function(key, traveler) {
                var addedBy = traveler.added_by ? '   (adaugat de: ' + traveler.added_by + ')' : '';
                var displayValue = traveler.phone + ' ' + traveler.name + addedBy;
                travelersNames.push({
                    label: displayValue,
                    value: traveler.name,
                    data: traveler
                });
                travelersPhones.push({
                    label: displayValue,
                    value: traveler.phone,
                    data: traveler
                });
            });
            container.find('#traveler_name').autocomplete({
                source: travelersNames,
                select: function(event, row) {
                    container.find('#phone').val(row.item.data.phone);
                }
            });
            container.find('#phone').autocomplete({
                source: travelersPhones,
                select: function(event, row) {
                    container.find('#traveler_name').val(row.item.data.name);
                }
            });
        });

        // seats spinner
        container.find('#seats').spinner({
            min: 1,
            max: 20,
            spin: function(ev, obj) {
                container.find('#total_price').val(obj.value * currentTrip.price);
            }
        });
        container.find('#seats').spinner('value', 1);
        container.find('#total_price').val(currentTrip.price);

        //container.find('#id').val(tripRowId);
        container.find('#trip_id').val(tripId);

        // complete known values
        if (!isNewTraveler) {
            container.find('#_method').val('PUT');
            container.find('#traveler_id').val(traveler.id);
            container.find('#traveler_name').val(traveler.name);
            container.find('#phone').val(traveler.phone);
            container.find('#seats').val(traveler.seats);
            container.find('#departure_city').val(traveler.from_city);
            container.find('#destination_city').val(traveler.to_city);
            container.find('#amount_paid').val(traveler.paid);
            container.find('#plane_connection').val(traveler.plane_connection);
            container.find('#plane_time').val(traveler.plane_time);
            container.find('#details').val(traveler.details);
        }

        // add functionality to save button
        container.find('.save-traveler').on('click',
            function(event) {
                /* global dataManager */
                var formValues = container.find('form').serialize();
                // save trip to db
                dataManager.addTravelerToTrip(formValues, function(data, status) {
                    switch (status) {
                        case 'success':
                            container.removeClass('well');
                            location.hash = '#trip/' + tripId;
                            break;
                        case 'error':
                            modal.show({
                                body:           dictionary.get($.parseJSON(data.responseText).message),
                                cancelText:     'Ok',
                                showConfirmBtn: false
                            });
                            break;
                    }
                });
                // add trip to js store
            }
        );

    },

    initLockTraveler: function(tripId, travelerId) {
        modal.show({
            'title':    '',
            'body':     'Sunteti sigur ca vreti sa Rezervati calatorul? Dupa rezervare veti putea imprima tichetul, dar nu il veti mai putea edita sau sterge!',
            'confirmCallback':  function() {
                schedule.lockTraveler(tripId, travelerId);
            }
        });
    },

    lockTraveler: function(tripId, travelerId) {
        var postData = {
            'traveler_id':  travelerId,
            'trip_id':      tripId,
            'lock':         true,
            '_token':       $('input[name="_token"]:first').val()
        };
        dataManager.lockTraveler(postData, function(data, status) {
            switch (status) {
                case 'success':
                    var button = $('[onclick="schedule.initLockTraveler(' + tripId + ',' + travelerId + ');"]');
                    button.attr('onclick', 'schedule.printTraveler(' + tripId + ',' + travelerId + ');"]');
                    button.html('<i class="glyphicon glyphicon-print"></i>' + dictionary.get('print'));
                    button.siblings('a[href="#editTraveler/' + tripId + '/' + travelerId + '"]').hide();
                    button.siblings('button[onclick="schedule.deleteTraveler(' + tripId + ',' + travelerId + ');"]').hide();
                    break;
                case 'error':
                    modal.show({
                        body:           dictionary.get($.parseJSON(data.responseText).message),
                        cancelText:     'Ok',
                        showConfirmBtn: false
                    });
                    break;
            }
        });
    },

    initUnlockTraveler: function(tripId, travelerId) {
        modal.show({
            'title':    '',
            'body':     'Sunteti sigur ca vreti sa Anulati Rezervarea pentru acest calator?',
            'confirmCallback':  function() {
                schedule.unlockTraveler(tripId, travelerId);
            }
        });
    },

    unlockTraveler: function(tripId, travelerId) {
        var postData = {
            'traveler_id':  travelerId,
            'trip_id':      tripId,
            'lock':         false,
            '_token':       $('input[name="_token"]:first').val()
        };
        dataManager.lockTraveler(postData, function(data, status) {
            switch (status) {
                case 'success':
                    var button = $('[onclick="schedule.printTraveler(' + tripId + ',' + travelerId + ');"]');
                    button.attr('onclick', 'schedule.initLockTraveler(' + tripId + ',' + travelerId + ');');
                    button.html('<i class="glyphicon glyphicon-lock"></i>' + dictionary.get('lock'));
                    button.siblings('a[href="#editTraveler/' + tripId + '/' + travelerId + '"]').removeClass('hidden');
                    button.siblings('button[onclick="schedule.deleteTraveler(' + tripId + ',' + travelerId + ');"]').removeClass('hidden');
                    break;
                case 'error':
                    modal.show({
                        body:           dictionary.get($.parseJSON(data.responseText).message),
                        cancelText:     'Ok',
                        showConfirmBtn: false
                    });
                    break;
            }
        });
    },

    deleteTraveler: function(tripId, travelerId) {
        modal.show({
            'title':    '',
            'body':     'Sunteti sigur ca doriti sa STERGETI calatorul?',
            'confirmCallback':  function() {
                schedule.doDeleteTraveler(tripId, travelerId);
            }
        });
    },

    doDeleteTraveler: function(tripId, travelerId) {
        var postData = {
            'traveler_id':  travelerId,
            'trip_id':      tripId,
            '_method':      'DELETE',
            '_token':       $('input[name="_token"]:first').val()
        };
        dataManager.addTravelerToTrip(postData, function(data, status) {
            switch (status) {
                case 'success':
                    location.hash = '#trip/' + tripId;
                    $(window).trigger('hashchange');
                    break;
                case 'error':
                    modal.show({
                        body:           dictionary.get($.parseJSON(data.responseText).message),
                        cancelText:     'Ok',
                        showConfirmBtn: false
                    });
                    break;
            }
        });
    },

    initMoveTravelers: function() {
        var tripName = currentTrip.title;
        var tripDate = customTime.format(currentTrip.startDate, 'date');
        modal.show({
            'title':    '',
            'body':     'Sunteti sigur ca doriti sa mutati calatorii pe transportul ' + tripName + ' in data ' + tripDate + ' ?',
            'confirmCallback':  function() {
                schedule.moveTravelers();
            }
        });
    },

    moveTravelers: function() {
        var travelers = [];
        $('.traveler-selected[aria-level="traveler"]').each(function(){
            travelers.push($(this).data('traveler-id'));
        });
        var postData = {
            'travelers':    travelers,
            'from_trip_id': currentTrip.id,
            'to_trip_id':   $('#move-travelers-select').val(),
            '_method':      'POST',
            '_token':       $('input[name="_token"]:first').val()
        };
        dataManager.moveTravelers(postData, function(data, status) {
            switch (status) {
                case 'success':
                    location.hash = '#trip/' + currentTrip.id;
                    $(window).trigger('hashchange');
                    break;
                case 'error':
                    modal.show({
                        body:           dictionary.get($.parseJSON(data.responseText).message),
                        cancelText:     'Ok',
                        showConfirmBtn: false
                    });
                    break;
            }
        });
    },

    printTraveler: function(tripId, travelerId) {
        var traveler = $.grep(currentTrip.travelers, function(n) {return n['id'] == travelerId;})[0];
        var template = $('.xtemplate#printTraveler').html();
        var dataForTemplate = {
            'trip':     currentTrip,
            'traveler': traveler
        };
        var printContents = templates.render(template, dataForTemplate);
        var w=window.open();
        w.document.write(printContents);
        w.print();
        w.close();
    },

    displayVehicles: function(vehicles) {
        var titleContainer = $("#tripDetailsTitle");
        titleContainer.html(dictionary.get('vehicles title'));

        var container = $("#tripDetails");
        var template = $('.xtemplate#displayVehicles').html();
        var content = templates.render(template, {
            'vehicles': vehicles
        });

        container.html(content);
    },

    editVehicle: function(id, vehicle) {
        /* global templates */
        var newVehicle = true;

        if (parseInt(id)) {
            newVehicle = false;
        } else {
            vehicle = schedule.newVehicle();
        }

        var template = $('.xtemplate#editVehicle').html();
        var titleContainer = $("#tripDetailsTitle");
        var container = $("#tripDetails");
        titleContainer.html(vehicle.name || dictionary.get('new vehicle title'));
        container.html(templates.render(template, vehicle));

        // set stored data
        if (!newVehicle) {
            container.find('#id').val(vehicle.id);
            container.find('#name').val(vehicle.name);
            container.find('#registration_number').val(vehicle.registration_number);
            container.find('#capacity').val(vehicle.capacity);
        }
        // add functionality to save button
        container.find('.save-vehicle').on('click',
            function(event) {
                /* global dataManager */
                var formValues = container.find('form').serialize();
                // save trip to db
                dataManager.addVehicle(formValues, function(data) {
                    location.hash = '#vehicles';
                });
                // add trip to js store
            }
        );
        // add functionality to save button
        container.find('.cancel-vehicle').on('click',
            function(event) {
                location.hash = '#vehicles';
            }
        );
    },

    displayDrivers: function(drivers) {
        var titleContainer = $("#tripDetailsTitle");
        titleContainer.html(dictionary.get('drivers title'));

        var container = $("#tripDetails");
        var template = $('.xtemplate#displayDrivers').html();
        var content = templates.render(template, {
            'drivers': drivers
        });

        container.html(content);
    },

    editDriver: function(id, driver) {
        /* global templates */
        var newDriver = true;

        if (parseInt(id)) {
            newDriver = false;
        } else {
            driver = schedule.newDriver();
        }

        var template = $('.xtemplate#editDriver').html();
        var titleContainer = $("#tripDetailsTitle");
        var container = $("#tripDetails");
        titleContainer.html(driver.name || dictionary.get('new driver title'));
        container.html(templates.render(template, driver));

        // set stored data
        if (!newDriver) {
            container.find('#id').val(driver.id);
            container.find('#name').val(driver.name);
            container.find('#phone').val(driver.phone);
            container.find('#active').val(driver.active);
        }
        // add functionality to save button
        container.find('.save-driver').on('click',
            function(event) {
                /* global dataManager */
                var formValues = container.find('form').serialize();
                // save trip to db
                dataManager.addDriver(formValues, function(data) {
                    location.hash = '#drivers';
                });
                // add trip to js store
            }
        );
        // add functionality to save button
        container.find('.cancel-driver').on('click',
            function(event) {
                location.hash = '#drivers';
            }
        );
    }
};

function initDatePopover(selectedDate){
    var tripsForDay = schedule.getTripsByDate(selectedDate);
    var content = {};
    if(!tripsForDay || !tripsForDay.length){
        content.errors = [
            {'message': dictionary.get('no trips for this day')}
        ];
    }
    tripsForDay.forEach(function(trip){
        trip.displayStartDate = customTime.format(trip.startDate,'time');
        trip.classLocked = trip.locked ? 'well-material-grey-900' : '';
    });
    content.trips = tripsForDay;

    // add button for adding new trip
    content.newTrips = [{
        'date':     customTime.format(selectedDate, 'date'),
        'title':    dictionary.get('add new trip'),
    }];

    var listTemplate = $('.xtemplate#listTripsForDay').html();
    $('.popover .popover-content').html(templates.render(listTemplate, content));
}

function onDateSelected(e){
    $('#selectedDate').html(customTime.format(e.date, 'serverDate'));
    var popoverContainer = $('.datepicker table tr td.active');
    var popoverTemplate = ['<div class="timePickerWrapper popover fresh">',
        '<div class="arrow"></div>',
        '<div class="popover-content">',
        '</div>',
        '</div>'].join('');
    $(popoverContainer).prop('data-trigger', 'focus');
    $('#datePopoverContainer').html('');

    $(popoverContainer).popover({
        trigger: 'focus',
        content: '<div class="uil-ring-css" style="transform:scale(0.24);"><div></div></div>',
        template: popoverTemplate,
        placement: "bottom",
        html: true,
        container: '#datePopoverContainer'
    });
    $(popoverContainer).popover('show');

    var callback = function() {
        initDatePopover(e.date);
    };
    dataManager.getTripsByDay(e.date, callback);
}

$(window).load(function(){
    // This command is used to initialize some elements and make them work properly
//                $.material.init();

    var dateContainer = $('#date');
    dateContainer.on('click', function(){
        $('#datePopoverContainer > div').not('.fresh').remove();
        $('#datePopoverContainer .fresh').removeClass('fresh');
    });

    dateContainer.datepicker({
            startDate: "01/01/2015",
            weekStart: 1,
            todayBtn: "linked",
            language: "ro",
            todayHighlight: true/*,
             datesDisabled: ['06/06/2015', '06/21/2015']*/
        })
        .on('changeDate', onDateSelected);

    $(window).trigger('hashchange');

});
